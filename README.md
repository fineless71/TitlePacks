# Title Packs

For Legacy Fabric 1.7 to 1.8.9. This mod adds back the resource packs button to the title screen.

This mod replaces the realms button, so make sure not to use the "Replace Realms" option in Mod Menu.

![Screenshot](screenshot.png)
