package nodomain.titlepacks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.gui.screen.GameMenuScreen;
import net.minecraft.client.gui.screen.ResourcePackScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.ButtonWidget;

@Mixin(value = GameMenuScreen.class)
public class GameMenuMixin extends Screen {

	@Inject(at = @At("TAIL"),
	method = "buttonClicked(Lnet/minecraft/client/gui/widget/ButtonWidget;)V")
	private void btnClicked(ButtonWidget button, CallbackInfo info) {
		if (button.id == 82) {
			this.client.setScreen(new ResourcePackScreen(this));
		}
	}
}
