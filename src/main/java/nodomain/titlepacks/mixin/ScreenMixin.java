package nodomain.titlepacks.mixin;

import java.util.List;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.client.gui.screen.Screen;

@Mixin(value = Screen.class,
priority = 2000) /* Needs to be after Mod Menu */
public class ScreenMixin {
	@Shadow
	private List<ButtonWidget> buttons;

	@Unique
	private String getBtnText() {
		String btnText = I18n.translate("options.resourcepack");
		if (btnText.length() >= 3 && btnText.endsWith("..."))
			btnText = btnText.substring(0, btnText.length() - 3);
		return btnText;
	}

	/* We have to inject directly into screen as that is how mod menu does it, and we need to load after them */
	@Inject(at = @At("TAIL"),
	method = "init(Lnet/minecraft/client/MinecraftClient;II)V")
	private void screenInit(MinecraftClient client, int width, int height, CallbackInfo ci) {
		for (ButtonWidget btn : this.buttons) {
			if (btn.message.equals(I18n.translate("menu.online"))) {
				btn.message = getBtnText();
				btn.id = 81;
				break;
			} else if (btn.message.equals(I18n.translate("menu.shareToLan"))) {
				if (!btn.active) {
					btn.active = true;
					btn.message = getBtnText();
					btn.id = 82;
				}
				break;
			}
		}
	}
}
