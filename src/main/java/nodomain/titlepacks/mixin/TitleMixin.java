package nodomain.titlepacks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.gui.screen.ResourcePackScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.client.gui.widget.ButtonWidget;

@Mixin(value = TitleScreen.class)
public class TitleMixin extends Screen {

	@Inject(at = @At("TAIL"),
	method = "buttonClicked(Lnet/minecraft/client/gui/widget/ButtonWidget;)V")
	private void btnClicked(ButtonWidget button, CallbackInfo info) {
		if (button.id == 81) {
			this.client.setScreen(new ResourcePackScreen(this));
		}
	}

	/*@ModifyConstant(method = "initWidgetsNormal(II)V")
	private String replaceRealmsText(String text) {
		if (text == "menu.online") {
			// Kinda jank because it's double translating. If the text is also a lang key, then it will be affected
			String btnText = I18n.translate("options.resourcepack");
			if (btnText.length() >= 3 && btnText.endsWith("..."))
				btnText = btnText.substring(0, btnText.length() - 3);
			return btnText;
		}
		return text;
	}*/

	/*@Inject(at = @At("TAIL"),
	method = "initWidgetsNormal(II)V")
	private void replaceRealms(int y, int spacingY, CallbackInfo info) {
		if (this.buttons.removeIf(btn -> btn instanceof ButtonWidget && ((ButtonWidget)btn).id == 14)) {
			String btnText = I18n.translate("options.resourcepack", new Object[0]);
			if (btnText.length() >= 3 && btnText.endsWith("..."))
				btnText = btnText.substring(0, btnText.length() - 3);
			this.buttons.add(new ButtonWidget(81, this.width / 2 - 100, y + spacingY * 2, btnText));
		}
	}*/
}
